package extract_assembly

import (
	"encoding/binary"
	"fmt"
	"io"
	"os"
	"path"
	"zipSame/util"
)

func TestExtractBlock() {
	filePathSrc := "/Users/wdfc/Downloads/ZipSameTest/LanJingApk/lanjing-40100.apk"
	filePathTarget := "/Users/wdfc/Downloads/ZipSameTest/LanJingApk/lanjing-40101.apk"
	filePathExtract := "/Users/wdfc/Downloads/ZipSameTest/LanJingApk-temp/lanjing-40101.apk.zst"
	filePathTargetReborn := "/Users/wdfc/Downloads/ZipSameTest/LanJingApk-reborn/lanjing-40101.apk"
	filePathExtractReborn := "/Users/wdfc/Downloads/ZipSameTest/LanJingApk-temp/lanjing-40101-reborn.apk.zst"
	blockSize := 32768 // 32kb
	// 提取目标文件与源文件不同的字节块存为结果文件
	Extract(filePathSrc, filePathTarget, filePathExtract, blockSize, true)
	// 组装
	Assembly(filePathSrc, filePathExtract, filePathTargetReborn, blockSize, true)
	// 检查是否一致，一致的话文件 filePathExtractReborn 是 0字节的，可以删除
	Extract(filePathTarget, filePathTargetReborn, filePathExtractReborn, blockSize, true)
}

func Extract(filePathSrc string, filePathTarget string, filePathExtract string, blockSize int, verbose bool) bool {
	if verbose {
		defer util.TimeCost()("此次提取") //注意，是对 timeCost()返回的函数进行调用，因此需要加两对小括号
		fmt.Println("\nInfo：开始提取不同的文件块：")
	}
	if blockSize < 0 { // 小于 8 kb
		if verbose {
			fmt.Println("Error：分块最小为 8 kb，当前参数是：", blockSize)
		}
	} else if !util.FileExist(filePathSrc) {
		if verbose {
			fmt.Println("Error：源文件不存在：", filePathSrc)
		}
	} else if !util.FileExist(filePathTarget) {
		if verbose {
			fmt.Println("Error：目标文件不存在：", filePathTarget)
		}
	} else {
		fileSrc, err := os.OpenFile(filePathSrc, os.O_RDONLY, os.ModePerm)
		if err != nil {
			if verbose {
				fmt.Println("Error：读入源文件失败：", filePathSrc, "错误是：", err)
			}
		} else {
			defer fileSrc.Close()

			fileTarget, err := os.OpenFile(filePathTarget, os.O_RDONLY, os.ModePerm)
			if err != nil {
				if verbose {
					fmt.Println("Error：读入目标文件失败：", filePathTarget, "错误是：", err)
				}
			} else {
				defer fileTarget.Close()

				// 创建文件夹
				fileDirExtract := path.Dir(filePathExtract)
				if !util.FileExist(fileDirExtract) {
					err := os.MkdirAll(fileDirExtract, os.ModePerm)
					if err != nil {
						if verbose {
							fmt.Println("Error：创建提取文件夹失败：", fileDirExtract, "错误是：", err)
						}
						return false
					}
				}
				// 删除提取文件
				err = os.Remove(filePathExtract)
				if err != nil && !os.IsNotExist(err) { // 忽略文件不存在的错误
					if verbose {
						fmt.Println("Error：删除提取文件失败：", filePathExtract, "，请检查是否已被打开，错误是：", err)
					}
					return false
				}
				// 创建提取文件，只写
				fileTemp, err := os.OpenFile(filePathExtract, os.O_CREATE|os.O_WRONLY, os.ModeAppend|os.ModePerm)
				if err != nil {
					if os.IsPermission(err) {
						fmt.Println("Error：没有权限创建提取文件：", filePathExtract, "，请检查是否文件夹不存在。错误是：", err)
					} else {
						fmt.Println("Error：创建提取文件失败：", filePathExtract, "。错误是：", err)
					}
				} else {
					defer fileTemp.Close()

					blockSrc := make([]byte, blockSize)
					blockTarget := make([]byte, blockSize)
					bufferForBlockIndex := make([]byte, binary.MaxVarintLen64)
					var blockIndex int64 = 0 // 当前处理的是第几块
					// 回到开始位置，开始比较
					_, err = fileSrc.Seek(0, 1)
					if err != nil {
						if verbose {
							fmt.Println("Error：源文件寻址到起始位置失败：", filePathSrc, "，错误是：", err)
						}
						return false
					}
					_, err = fileTarget.Seek(0, 1)
					if err != nil {
						if verbose {
							fmt.Println("Error：目标文件寻址到起始位置失败：", filePathTarget, "，错误是：", err)
						}
						return false
					}
					fileSizeTargetSum := 0
					for {
						readLengthTarget, err := fileTarget.Read(blockTarget)
						if err != nil && err != io.EOF {
							if verbose {
								fmt.Println("Error：读入目标文件失败：", filePathTarget, "，错误是：", err)
							}
							return false
						}
						if 0 == readLengthTarget { // 目标文件已经没有数据，可以退出这个 for 了
							break
						} else {
							fileSizeTargetSum += readLengthTarget
							readLengthSrc, err := fileSrc.Read(blockSrc)
							if err != nil && err != io.EOF {
								if verbose {
									fmt.Println("Error：读入源文件失败：", filePathSrc, "，错误是：", err)
								}
								return false
							}
							same := readLengthTarget == readLengthSrc // 长度不一致，两个 block 不是一样的
							if same {                                 // 一样长度，看看每一个值是否一致
								for index := 0; index < readLengthSrc; index++ {
									if blockSrc[index] != blockTarget[index] { // 一个值不一致，说明这个块已经无效了
										same = false
										break
									}
								}
							}
							if !same { // 不同则把读入长度的数据存入临时文件
								if verbose {
									fmt.Println("Info：第", blockIndex, "块不同，已读入总长度：\t\t", fileSizeTargetSum, "，读入字节块大小为：", readLengthTarget)
								}
								binary.PutVarint(bufferForBlockIndex, blockIndex)
								written, err := fileTemp.Write(bufferForBlockIndex) // 先记录第几块，用的缓冲区长度是 binary.MaxVarintLen64 就是 10
								if err != nil || binary.MaxVarintLen64 != written {
									if verbose {
										fmt.Println("Error：写入提取文件失败：", filePathExtract, "，错误是：", err)
									}
									return false
								}
								if readLengthTarget > 0 {
									writeReadBlockToFile(blockTarget, blockSize, readLengthTarget, *fileTemp)
								}
							}
							blockIndex++
						}
					}
					if verbose {
						fmt.Println("Info：提取不同的文件块完成，\n -- 源文件：", filePathSrc, "\n -- 目标文件(大小：", fileSizeTargetSum, ")  ", filePathTarget, "\n -- 提取的文件：", filePathExtract)
					}
					return true
				}
			}
		}
	}
	if verbose {
		fmt.Println("Error：提取不同的文件块任务失败")
		fmt.Println("\n -- 源文件：", filePathSrc, "\n -- 目标文件", filePathTarget, "\n -- 提取的文件：", filePathExtract)
	}
	return false
}

func Assembly(filePathSrc string, filePathExtract string, filePathTarget string, blockSize int, verbose bool) bool {
	if verbose {
		defer util.TimeCost()("此次组装") //注意，是对 timeCost()返回的函数进行调用，因此需要加两对小括号
		fmt.Println("\nInfo：开始组装文件：")
	}
	if blockSize < 0 { // 小于 8 kb
		if verbose {
			fmt.Println("Error：分块最小为 8 kb，当前参数是：", blockSize)
		}
	} else if !util.FileExist(filePathSrc) {
		if verbose {
			fmt.Println("Error：源文件不存在：", filePathSrc)
		}
	} else if !util.FileExist(filePathExtract) {
		if verbose {
			fmt.Println("Error：提取文件不存在：", filePathExtract)
		}
	} else {
		fileSrc, err := os.OpenFile(filePathSrc, os.O_RDONLY, os.ModePerm)
		if err != nil {
			if verbose {
				fmt.Println("Error：读入源文件失败：", filePathSrc, "错误是：", err)
			}
		} else {
			defer fileSrc.Close()

			fileExtract, err := os.OpenFile(filePathExtract, os.O_RDONLY, os.ModePerm)
			if err != nil {
				if verbose {
					fmt.Println("Error：读入提取文件失败：", filePathExtract, "错误是：", err)
				}
			} else {
				defer fileExtract.Close()

				// 创建文件夹
				fileDirTarget := path.Dir(filePathTarget)
				if !util.FileExist(fileDirTarget) {
					err := os.MkdirAll(fileDirTarget, os.ModePerm)
					if err != nil {
						if verbose {
							fmt.Println("Error：创建目标文件夹失败：", fileDirTarget, "错误是：", err)
						}
						return false
					}
				}
				// 删除目标文件
				err = os.Remove(filePathTarget)
				if err != nil && !os.IsNotExist(err) { // 忽略文件不存在的错误
					if verbose {
						fmt.Println("Error：删除目标文件失败：", filePathTarget, "，请检查是否已被打开，错误是：", err)
					}
					return false
				}
				// 创建目标文件，只写
				fileTarget, err := os.OpenFile(filePathTarget, os.O_CREATE|os.O_WRONLY, os.ModeAppend|os.ModePerm)
				if err != nil {
					if os.IsPermission(err) {
						fmt.Println("Error：没有权限创建目标文件：", filePathTarget, "，请检查是否文件夹不存在。错误是：", err)
					} else {
						fmt.Println("Error：创建目标文件失败：", filePathTarget, "。错误是：", err)
					}
				} else {
					defer fileTarget.Close()

					blockSrc := make([]byte, blockSize)
					blockTarget := make([]byte, blockSize)
					bufferForBlockIndexTarget := make([]byte, binary.MaxVarintLen64)
					var blockIndexSrc int64 = 0    // 当前处理的是第几块
					var blockIndexTarget int64 = 0 // 当前处理的是第几块
					// 源文件回到开始位置，开始比较
					_, err = fileSrc.Seek(0, 1)
					if err != nil {
						if verbose {
							fmt.Println("Error：源文件寻址到起始位置失败：", filePathSrc, "，错误是：", err)
						}
						return false
					}
					fileInfoSrc, err := fileSrc.Stat()
					if err != nil {
						if verbose {
							fmt.Println("Error：读入源文件状态失败：", filePathSrc, "，错误是：", err)
						}
						return false
					}
					blockIndexTargetMax := fileInfoSrc.Size()/int64(blockSize) + 1 // 有可能末尾不够 blockSize 大小，但还是有少量数据，所以加一
					fileSizeTargetSum := 0
					for {
						// 读入 bufferForBlockIndex，解析出来是第几块
						readLengthTarget, err := fileExtract.Read(bufferForBlockIndexTarget)
						if err != nil && err != io.EOF {
							if verbose {
								fmt.Println("Error：读入提取文件失败：", filePathExtract, "，错误是：", err)
							}
							return false
						}
						if 0 == readLengthTarget { // 目标文件已经没有数据，把剩下的源文件的块复制进目标文件，就退出这个 for
							if blockIndexTarget < blockIndexTargetMax {
								blockIndexTarget = blockIndexTargetMax
							} else {
								if verbose {
									fmt.Println("Info：组装完毕，目标文件大小：\t\t\t", fileSizeTargetSum, " 源文件总块数: ", blockIndexSrc, " 目标文件总块数: ", blockIndexTarget)
									fmt.Println(" -- 源文件：", filePathSrc, "\n -- 提取的文件：", filePathExtract, "\n -- 组装出来的目标文件(大小：", fileSizeTargetSum, ") ", filePathTarget)
								}
								return true
							}
						} else {
							blockIndexTarget, _ = binary.Varint(bufferForBlockIndexTarget)
							readLengthTarget, err = fileExtract.Read(blockTarget) // 接着读入一个 blockSize 大小的内容
							if err != nil && err != io.EOF {
								if verbose {
									fmt.Println("Error：读入提取文件失败：", filePathExtract, "，错误是：", err)
								}
								return false
							}
							if 0 == readLengthTarget { // 目标文件已经没有数据，把剩下的源文件的块复制进目标文件，就退出这个 for
								blockIndexTarget = blockIndexTargetMax
							}
						}
						for ; blockIndexSrc < blockIndexTarget; blockIndexSrc++ { // 目标块之前的块都从源文件 fileSrc 读入，组装到目标文件 fileTarget 去
							readLengthSrc, err := fileSrc.Read(blockSrc)
							if err != nil && err != io.EOF {
								if verbose {
									fmt.Println("Error：读入源文件失败：", filePathSrc, "，错误是：", err)
								}
								return false
							}
							if readLengthSrc > 0 {
								writeReadBlockToFile(blockSrc, blockSize, readLengthSrc, *fileTarget) // 存入目标文件
								fileSizeTargetSum += readLengthSrc
							}
						}
						if verbose {
							fmt.Println("Info：顺序复制源文件块组装到目标文件，当前大小：", fileSizeTargetSum, " 源文件第 ", blockIndexSrc-1, " 块，目标文件第 ", blockIndexTarget, " 块")
						}
						if 0 != readLengthTarget { // 这块数据接着插入
							writeReadBlockToFile(blockTarget, blockSize, readLengthTarget, *fileTarget) // 存入目标文件
							blockIndexSrc++                                                             // 应该跳到下个，因为这个被 fileTarget 取出的块给替换了，所以下一个才是需要的
							_, err = fileSrc.Seek(int64(blockSize), 1)                                  // 从当前位置偏移 blockSize 大小，就是跳过这个块
							if err != nil {
								if verbose {
									fmt.Println("Error：源文件寻址到指定位置 ", blockSize, " 失败：", filePathSrc, "，错误是：", err)
								}
								return false
							}
							fileSizeTargetSum += readLengthTarget
							if verbose {
								fmt.Println("Info：复制提取文件一块组装到目标文件，当前大小：", fileSizeTargetSum, " 换源件第 ", blockIndexSrc-1, " 块，到目标件第 ", blockIndexTarget, " 块")
							}
						}
					}
				}
			}
		}
	}
	if verbose {
		fmt.Println("Error：组装文件任务失败")
		fmt.Println(" -- 源文件：", filePathSrc, "\n -- 提取的文件：", filePathExtract, "\n -- 目标文件", filePathTarget)
	}
	return false
}

func writeReadBlockToFile(block []byte, blockSize int, readSize int, file os.File) {
	readBlock := block         // 读入的和缓冲区一样多，直接存入
	if readSize != blockSize { // 读入的比缓冲区少，就只复制精确的内容到一个新缓冲区里再存入
		readBlock = make([]byte, readSize)
		copy(readBlock, block[0:readSize])
	}
	written, err := file.Write(readBlock)
	if err != nil || readSize != written {
		panic(err)
	}
}
