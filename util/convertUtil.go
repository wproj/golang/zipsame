package util

import (
	"bytes"
	"encoding/binary"
)

// 32位整形转换成字节
func IntToBytes(n int) []byte {
	x := int32(n)
	bytesBuffer := bytes.NewBuffer([]byte{})
	err := binary.Write(bytesBuffer, binary.BigEndian, x)
	if err != nil {
		panic(err)
	}
	return bytesBuffer.Bytes()
}

// 字节转换成 32位整形
func bytesToInt(b []byte) int {
	bytesBuffer := bytes.NewBuffer(b)
	var x int32
	err := binary.Read(bytesBuffer, binary.BigEndian, &x)
	if err != nil {
		panic(err)
	}
	return int(x)
}

// 64位整形转换成字节
func int64ToBytes(n int64) []byte {
	buf := make([]byte, binary.MaxVarintLen64)
	binary.PutVarint(buf, n)
	return buf
}

// 字节转换成 64位整形
func bytesToInt64(buf []byte) int64 {
	n, _ := binary.Varint(buf)
	return n
}
