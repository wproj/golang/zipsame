package util

import (
	"archive/zip"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func FileExist(filePath string) bool { // 检查文件是否存在，存在则返回具体信息
	_, err := os.Stat(filePath)
	if err != nil {
		if os.IsNotExist(err) { // 文件不存在，返回 false
			return false
		} else {
			panic(err)
		}
	}
	return true // 没有出错，返回 true
}

func IsDir(filePath string) bool { // 判断所给路径是否为文件夹
	stat, err := os.Stat(filePath)
	if err != nil {
		if os.IsNotExist(err) { // 文件不存在，返回 false
			return false
		} else {
			panic(err)
		}
	}
	return stat.IsDir() // 没有出错，返回
}

func FileInfo(filePath string) string { // 获取文件的一些信息，如果存在则返回字符串
	fileInfo, err := os.Stat(filePath)
	if err != nil {
		fmt.Println("文件: ", filePath, " 不存在")
	} else {
		var builder strings.Builder
		builder.WriteString("File name:")
		builder.WriteString(fileInfo.Name())
		builder.WriteString("\n")
		builder.WriteString("Size in bytes:")
		builder.WriteString(strconv.FormatInt(fileInfo.Size(), 10))
		builder.WriteString("\n")
		builder.WriteString("Permissions:" + fileInfo.Mode().String())
		builder.WriteString("\n")
		builder.WriteString("Last modified:")
		builder.WriteString(fileInfo.ModTime().String())
		builder.WriteString("\n")
		builder.WriteString("Is Directory: ")
		if fileInfo.IsDir() {
			builder.WriteString("yes")
		} else {
			builder.WriteString("no")
		}
		builder.WriteString("\n")
		builder.WriteString(fmt.Sprintf("System interface type: %T\n", fileInfo.Sys()))
		builder.WriteString(fmt.Sprintf("System info: %+v\n\n", fileInfo.Sys()))
		return builder.String()
	}
	return ""
}

func FileSize(filePath string) int64 { // 获取文件的长度，如果存在则返回 int64 值
	fileInfo, err := os.Stat(filePath)
	if err != nil {
		fmt.Println("文件: ", filePath, " 不存在")
	} else {
		return fileInfo.Size()
	}
	return 0
}

func ChangeMode(filePath string) { // 改变linux下的文件权限
	err := os.Chmod(filePath, 0777)
	if err != nil {
		panic(err)
	}
}

// 来自 https://blog.csdn.net/wade3015/article/details/90245919
func CopyFileByBuffer(srcPath string, targetPath string, bufferSize int) error {
	//打开源文件
	fileRead, err := os.Open(srcPath)
	if err != nil {
		fmt.Println("Open err:", err)
		return err
	}
	defer fileRead.Close()
	//创建目标文件
	fileWrite, err := os.Create(targetPath)
	if err != nil {
		fmt.Println("Create err:", err)
		return err
	}
	defer fileWrite.Close()

	//从源文件获取数据，放到缓冲区
	buf := make([]byte, 4096)
	//循环从源文件中获取数据，全部写到目标文件中
	for {
		n, err := fileRead.Read(buf)
		if err != nil && err == io.EOF {
			return nil
		}
		fileWrite.Write(buf[:n]) //读多少、写多少
	}
	return nil
}

func WriteByteToFile(filePath string, content []byte) error {

	file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, os.ModeAppend|os.ModePerm)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	defer file.Close()
	_, err = file.Write(content)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	return nil
}

func ReadByteFromFile(path string) ([]byte, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	return ioutil.ReadAll(f)
}


// srcFile could be a single file or a directory
func Zip(srcFile string, destZip string) error {
	zipfile, err := os.Create(destZip)
	if err != nil {
		return err
	}
	defer zipfile.Close()

	archive := zip.NewWriter(zipfile)
	defer archive.Close()

	filepath.Walk(srcFile, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		header, err := zip.FileInfoHeader(info)
		if err != nil {
			return err
		}


		header.Name = strings.TrimPrefix(path, filepath.Dir(srcFile) + "/")
		// header.Name = path
		if info.IsDir() {
			header.Name += "/"
		} else {
			header.Method = zip.Deflate
		}

		writer, err := archive.CreateHeader(header)
		if err != nil {
			return err
		}

		if ! info.IsDir() {
			file, err := os.Open(path)
			if err != nil {
				return err
			}
			defer file.Close()
			_, err = io.Copy(writer, file)
		}
		return err
	})

	return err
}

func Unzip(zipFile string, destDir string) error {
	zipReader, err := zip.OpenReader(zipFile)
	if err != nil {
		return err
	}
	defer zipReader.Close()

	for _, f := range zipReader.File {
		fpath := filepath.Join(destDir, f.Name)
		if f.FileInfo().IsDir() {
			os.MkdirAll(fpath, os.ModePerm)
		} else {
			if err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm); err != nil {
				return err
			}

			inFile, err := f.Open()
			if err != nil {
				return err
			}
			defer inFile.Close()

			outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
			if err != nil {
				return err
			}
			defer outFile.Close()

			_, err = io.Copy(outFile, inFile)
			if err != nil {
				return err
			}
		}
	}
	return nil
}