package util

import (
	"fmt"
	"time"
)

// 优雅计算执行耗时，来自：https://www.cnblogs.com/unqiang/p/11757086.html

//@brief：耗时统计函数，用法：  defer timeCost()()  //注意，是对 timeCost()返回的函数进行调用，因此需要加两对小括号
func TimeCost() func(string) {
	start := time.Now()
	return func(msg string) {
		tc:=time.Since(start)
		fmt.Printf("%s 执行耗时 = %v\n", msg, tc)
	}
}