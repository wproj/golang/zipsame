package main

import (
	"flag"
	"fmt"
	"os"
	"path"
)

var VERSION string = "v0.3"

var (
	h bool  // hello
	v bool  // version
	V *bool // Verbose

	u bool   // unzip
	d string // directory name at current path
	D string // directory full path
	z string // zip file name at current path
	Z string // zip file full path
	b int    // block size
	c string // compare file name
)

func init() {
	initFlag()
}

func main() {
	flag.Parse()

	/* // TODO 以下为测试用
	//fmt.Println("\n开始测试提取和组装功能")
	//extract_assembly.TestExtractBlock()
	fmt.Println("\n开始测试压缩和解压功能")
	testZipAndUnZipSame()
	return
	// TODO 以上为测试用
	*/

	// TODO 以下模拟单个参数 zip
	// D = "/Users/wdfc/Downloads/ZipSameTest/LanJingApk"
	// TODO 以下模拟两个参数 unZip , 需要改  if u {   为  if !u {
	// D = "/Users/wdfc/Downloads/ZipSameTest/LanJingApk-1"
	// Z = "/Users/wdfc/Downloads/ZipSameTest/LanJingApk.zip"

	if h {
		flag.Usage()
	} else if v {
		fmt.Println("当前版本号", VERSION)
	} else {
		dirPath := ""
		if len(D) > 0 { // 有路径就忽略 d 提供的源文件夹名字
			dirPath = D
		} else if len(d) > 0 {
			currentDir, err := os.Getwd()
			if err != nil {
				fmt.Println("获取当前目录出错，无法设置当前目录下的文件夹，任务中断。错误是：", err)
				return
			} else {
				dirPath = path.Join(currentDir, d)
			}
		}
		zipFilePath := ""
		if len(Z) > 0 { // 有路径就忽略 z 提供的目标文件名字
			zipFilePath = Z
		} else if len(z) > 0 {
			currentDir, err := os.Getwd()
			if err != nil {
				fmt.Println("获取当前目录出错，无法获取当前目录下的 zip 文件路径，任务中断。错误是：", err)
				return
			} else {
				zipFilePath = path.Join(currentDir, z)
			}
		}
		if u { // TODO 测试 unZip 时改为： if !u {
			if len(zipFilePath) > 0 { // 解压缩只要有 zip 文件路径就行
				unZipSame(zipFilePath, dirPath, *V)
			} else {
				fmt.Println("解压缩需要 zip 文件名或路径，请用 -h 查看帮助")
			}
		} else if len(dirPath) > 0 { // 压缩只要有源文件夹路径就行
			zipSame(dirPath, zipFilePath, c, b, *V)
		} else {
			fmt.Println("\n缺少参数：文件夹名或路径。或用 -h 查看帮助")
		}
	}
}

func initFlag() {
	flag.BoolVar(&h, "h", false, "压缩示例： ./zipSame -D /xxx/ApkDir -z /xxx/ApkDir.zip\n解压示例： ./zipSame -u -z /xxx/ApkDir.zip -D /xxx/ApkDir-unzip")
	flag.BoolVar(&v, "v", false, "版本信息")
	// 另一种绑定方式
	V = flag.Bool("V", false, "设置 `verbose` 输出日志，默认不输出") // 示例另一种绑定方式
	flag.BoolVar(&u, "u", false, "解压缩文件，默认是压缩文件夹")
	flag.StringVar(&d, "d", "", "设置 `dirName` 作为当前目录下的文件夹名字")
	flag.StringVar(&D, "D", "", "设置 `dirPath` 作为文件夹的路径，有就忽略 d 提供的文件夹名字")
	flag.StringVar(&z, "z", "", "设置 `zipFileName` 作为当前目录下的 zip 文件")
	flag.StringVar(&Z, "Z", "", "设置 `zipFilePath` 作为 zip 文件的路径，有就忽略 z 提供的 zip 文件名字")
	// 注意 `blockSize`。默认是 -b blockSize，有了 `blockSize` 之后，变为 -b blockSize
	flag.IntVar(&b, "b", 32768, "设置 `blockSize` 作为文件块的切分大小，最小为 " + string(BLOCK_SIZE_MIN))
	// 注意 `compareFileName`。默认是 -c compareFileName，有了 `compareFileName` 之后，变为 -c compareFileName
	flag.StringVar(&c, "c", "", "设置 `compareFileName` 作为源文件夹里参照物文件的名字，默认为 d/D 文件夹里第一个有文件名的文件")
	// 改变默认的 Usage
	flag.Usage = printFlagUsage
}

func printFlagUsage() {
	_, err := fmt.Fprintf(os.Stderr, `
ZipSame version: ZipSame/%s
Usage: zipsame [-hvVu] [-D dirPath] [-d dirName] [-Z zipFilePath] [-z zipFileName] [-b blockSize] [-c compareFileName]

Options:
`, VERSION)
	if err != nil {
		fmt.Println("打印用法时出错：", err.Error())
	}
	flag.PrintDefaults()
}