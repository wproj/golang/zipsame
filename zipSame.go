package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
	"time"
	"zipSame/extract_assembly"
	"zipSame/util"
)


var ZIP_TEMP_FILE_EXTENSION = ".zst"        // zst 代表 zip same temp 文件
var ZIP_INSIDE_DIR_NAME = "zip_same_inside" // 压缩包里的文件夹名字
var BLOCK_SIZE_MIN = 8192					// 分块最小的是 8 kb

func testZipAndUnZipSame() {
	// 以下测试成功
	ss := "/Users/wdfc/Downloads/ZipSameTest/LanJingApk"
	tt := "/Users/wdfc/Downloads/ZipSameTest/LanJingApk.zip"
	cc := "lanjing-40100.apk"
	bb := 32768
	vv := false
	zipSame(ss, tt, cc, bb, vv)

	sss := "/Users/wdfc/Downloads/ZipSameTest/LanJinApk-reborn"
	unZipSame(tt, sss, vv)
}

func zipSame(sourceDir string, zipFilePath string, compareFileName string, blockSize int, verbose bool) {
	defer util.TimeCost()("此次压缩")

	fmt.Println("\n开始压缩文件夹")
	if !util.IsDir(sourceDir) || !util.FileExist(sourceDir) {
		fmt.Println("Error：来源不是文件夹或者不存在，任务中断")
	} else {
		sourceFileInfoList, err := ioutil.ReadDir(sourceDir)
		if err != nil {
			fmt.Println("Error：读取源文件夹出错，任务中断。错误为：", err)
		} else if len(sourceFileInfoList) == 0 {
			fmt.Println("Info：源文件夹里没有文件，无需 ZipSame，任务完成")
		} else if len(zipFilePath) > 0 && util.IsDir(zipFilePath) { // 目标文件路径为文件夹
			fmt.Println("Error：zip 文件路径为文件夹，必须是文件，任务中断")
		} else {
			if len(zipFilePath) == 0 { // 目标文件路径长度为 0
				// golang中使用time.Now().Format("2006/1/2 15:04:05") 格式化时间输出时,Format的参数必须是"2006/1/2 15:04:05",这个时间为例的时间格式?
				now := time.Now().Format("20060102-1504")
				sourceDirName := path.Base(sourceDir)
				currentDir, _ := os.Getwd()
				targetFileName := sourceDirName + "-" + now + ".zip"
				zipFilePath = path.Join(currentDir, targetFileName)
				fmt.Println("Info：未给出 zip 文件路径，设为：", zipFilePath)
			} else {
				targetFileExt := path.Ext(zipFilePath)
				if len(targetFileExt) == 0 || strings.Compare(targetFileExt, ".zip") != 0 {
					fmt.Println("Info：zip 文件后缀是 " + targetFileExt + " ，改为 .zip")
					zipFilePath = strings.Replace(zipFilePath, targetFileExt, ".zip", -1) //如果替换次数小于0，表示全部替换
				}
			}
			hasFile := false
			if len(compareFileName) == 0 || util.IsDir(compareFileName) {
				for _, fileInfo := range sourceFileInfoList {
					fileName := fileInfo.Name()
					if !strings.HasPrefix(fileName, ".") { // 避免是隐藏文件例如 .DS_Store
						compareFileName = fileName
						hasFile = true
						break
					}
				}
				if hasFile {
					fmt.Println("Info：未给出参照文件的名字或为文件夹，设为：", compareFileName)
				} else {
					fmt.Println("Info：源文件夹内没有可参照的文件，任务中断")
					return
				}
			} else {
				hasFile = false
				for _, fileInfo := range sourceFileInfoList {
					if strings.Compare(fileInfo.Name(), compareFileName) == 0 {
						hasFile = true
						break
					}
				}
				if !hasFile {
					compareFileName = sourceFileInfoList[0].Name()
					fmt.Println("Info：源文件夹没有给出参照文件，设为：", compareFileName)
				}
			}
			if blockSize < BLOCK_SIZE_MIN { // 小于 8 kb
				fmt.Println("Info：分块改为最小的 8 kb")
				blockSize = BLOCK_SIZE_MIN
			}

			currentDir, _ := os.Getwd()
			tempDirPath := path.Join(currentDir, ZIP_INSIDE_DIR_NAME)
			if util.FileExist(tempDirPath) {
				err := os.RemoveAll(tempDirPath) // RemoveAll 只要文件夹存在，删除文件夹 无论文件是否有内容都会删除 如果是文件则删除文件
				if err != nil {
					fmt.Println("Error：删除临时文件夹出错，任务中断。文件夹路径：", tempDirPath, "错误为：", err)
					return
				}
			}
			err := os.MkdirAll(tempDirPath, os.ModePerm)
			if err != nil {
				fmt.Println("Error：创建临时文件夹出错，任务中断。文件夹路径：", tempDirPath, "错误为：", err)
			} else {
				for _, fileInfo := range sourceFileInfoList {
					fileName := fileInfo.Name()
					compareFilePath := path.Join(sourceDir, compareFileName)
					sourceFilePath := path.Join(sourceDir, fileName)
					// 不处理参照物文件或 . 开头的例如隐藏文件 或 .DS_Store
					if strings.HasPrefix(fileName, ".") || strings.Compare(fileName, compareFileName) == 0 {
						filePath := path.Join(sourceDir, fileName)
						targetFilePath := path.Join(tempDirPath, fileName) // 不带后缀 ZIP_TEMP_FILE_EXTENSION
						err := util.CopyFileByBuffer(filePath, targetFilePath, blockSize)
						if err != nil {
							fmt.Println("Error：复制文件出错，任务中断。目标文件路径：", targetFilePath)
							return
						}
					} else {
						targetFilePath := path.Join(tempDirPath, fileName+ZIP_TEMP_FILE_EXTENSION)
						extract_assembly.Extract(compareFilePath, sourceFilePath, targetFilePath, blockSize, verbose)
					}
				}
				targetFileInfoList, err := ioutil.ReadDir(tempDirPath)
				if err != nil {
					fmt.Println("Error：读取临时文件夹出错，任务中断。错误为：", err)
				}
				if len(sourceFileInfoList) != len(targetFileInfoList) {
					fmt.Println("Error：压缩前的临时文件夹里文件数量和来源文件夹里的文件数量不相等，任务中断")
					return
				} else {
					var zipSameConfig ZipSameConfig
					zipSameConfig.CompareFileName = compareFileName
					zipSameConfig.BlockSize = blockSize
					configFilePath := path.Join(tempDirPath, "zipSameConfig.txt")
					configByte, err := json.Marshal(zipSameConfig)
					err = util.WriteByteToFile(configFilePath, configByte)
					if err != nil {
						fmt.Println("Error：在临时文件夹里写入配置文件出错，任务中断", err)
						return
					}
					err = util.Zip(tempDirPath, zipFilePath)
					if err != nil {
						fmt.Println("Error：压缩临时文件出错，任务中断", err)
						return
					} else {
						err = os.RemoveAll(tempDirPath)
						if err != nil {
							fmt.Println("Error：删除临时文件夹出错，任务已完成。文件夹路径：", tempDirPath, "错误为：", err)
						}
						fmt.Println("Info：压缩文件夹成功")
					}
				}
			}
		}
	}
}

func unZipSame(zipFilePath string, targetDir string, verbose bool) {
	defer util.TimeCost()("此次解压")

	fmt.Println("\n开始解压文件")
	if util.IsDir(zipFilePath) { //  zip 文件路径为文件夹
		fmt.Println("Error： zip 文件路径为文件夹，必须是 zip 文件，任务中断")
	} else if len(zipFilePath) == 0 || !util.FileExist(zipFilePath) {
		fmt.Println("Error： zip 文件不存在，任务中断")
	} else {
		sourceFileExt := path.Ext(zipFilePath)
		if len(sourceFileExt) == 0 || strings.Compare(sourceFileExt, ".zip") != 0 {
			fmt.Println("Info： zip 文件后缀是 " + sourceFileExt + " ，必须为 .zip，任务中断")
			return
		} else if len(targetDir) == 0 { // 目标文件夹路径长度为 0
			sourceFileExt := path.Ext(zipFilePath)
			sourceFileName := path.Base(zipFilePath)
			targetDirName := strings.Replace(sourceFileName, sourceFileExt, "", -1)
			currentDir, _ := os.Getwd()
			targetDir = path.Join(currentDir, targetDirName)
			fmt.Println("Error：目标文件夹不是文件夹或者不存在，改为：", targetDir)
		} else {
			if util.FileExist(targetDir) {
				err := os.RemoveAll(targetDir) // RemoveAll 只要文件夹存在，删除文件夹 无论文件是否有内容都会删除 如果是文件则删除文件
				if err != nil {
					fmt.Println("Error：删除目标文件夹出错，任务中断。文件夹路径：", targetDir, "错误为：", err)
					return
				}
			}
		}
		err := util.Unzip(zipFilePath, targetDir)
		if err != nil {
			fmt.Println("Error：解压缩文件出错，任务中断", err)
			return
		} else {
			zipInsideDirPath := path.Join(targetDir, ZIP_INSIDE_DIR_NAME)
			if !util.FileExist(zipInsideDirPath) {
				fmt.Println("Error：解压出来的文件夹里没有文件夹 ", ZIP_INSIDE_DIR_NAME, "，无法重组，任务中断。文件夹路径：", zipInsideDirPath, "错误为：", err)
				return
			}
			configFilePath := path.Join(zipInsideDirPath, "zipSameConfig.txt")
			configByte, err := util.ReadByteFromFile(configFilePath)
			if err != nil {
				fmt.Println("Error：在目标文件夹里读入配置文件出错，任务中断", err)
				return
			}
			var zipSameConfig ZipSameConfig
			err = json.Unmarshal(configByte, &zipSameConfig)
			if err != nil {
				fmt.Println("Error：把目标文件夹里读入的配置文件转换成配置数据出错，任务中断", err)
				return
			}
			if zipSameConfig.BlockSize < BLOCK_SIZE_MIN { // 小于 8 kb
				fmt.Println("Info：分块设置为最小的 8 kb")
				zipSameConfig.BlockSize = BLOCK_SIZE_MIN
			}
			if len(zipSameConfig.CompareFileName) == 0 {
				fmt.Println("Error：配置数据给出的参照文件名无效，任务中断")
				return
			} else {
				compareFilePath := path.Join(zipInsideDirPath, zipSameConfig.CompareFileName)
				if !util.FileExist(compareFilePath) {
					fmt.Println("Error：配置数据给出的参照文件不存在，任务中断")
					return
				}
				filepath.Walk(zipInsideDirPath, func(zipInsideFilePath string, info os.FileInfo, err error) error {
					if err != nil {
						fmt.Println("Error：重组目标文件", zipInsideFilePath, " 时出错，错误是：", err)
						return err
					}
					zipInserFileName := path.Base(zipInsideFilePath)
					// 不处理参照物文件或 . 开头的例如隐藏文件 或 .DS_Store
					if strings.HasPrefix(zipInserFileName, ".") || strings.Compare(zipInserFileName, zipSameConfig.CompareFileName) == 0 {
						targetFilePath := path.Join(targetDir, zipInserFileName)
						err := util.CopyFileByBuffer(zipInsideFilePath, targetFilePath, zipSameConfig.BlockSize)
						if err != nil {
							fmt.Println("Error：复制文件出错，任务中断。目标文件路径：", targetFilePath)
							return nil
						}
					} else if !util.IsDir(zipInsideFilePath) && strings.Compare(compareFilePath, zipInsideFilePath) != 0 && strings.Compare(configFilePath, zipInsideFilePath) != 0 {
						targetFileName := path.Base(zipInsideFilePath)
						targetFileName = strings.Replace(targetFileName, ZIP_TEMP_FILE_EXTENSION, "", -1)
						targetFilePath := path.Join(targetDir, targetFileName)
						extract_assembly.Assembly(compareFilePath, zipInsideFilePath, targetFilePath, zipSameConfig.BlockSize, verbose)
					}
					return nil
				})
				err = os.RemoveAll(zipInsideDirPath)
				if err != nil {
					fmt.Println("Info：删除解压缩后的临时文件夹 ", zipInsideDirPath, " 失败")
				}
				fmt.Println("Info：解压文件成功")
			}
		}
	}
}

type ZipSameConfig struct {
	CompareFileName string
	BlockSize int
}